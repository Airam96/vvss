package agenda.model.repository.classes;


import agenda.model.base.Activity;
import agenda.model.repository.interfaces.RepositoryActivity;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Clasa gestioneaza activitatiile mock pentru teste
 */
public class RepositoryActivityMock implements RepositoryActivity {

	private List<Activity> activities;
	
	public RepositoryActivityMock()
	{
		activities = new LinkedList<Activity>();
//		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
//		try {
//			Activity act = new Activity(df.parse("03/20/2013 12:00"), 
//					df.parse("03/20/2013 14:00"),
//					null,
//					"Meal break",
//					"Memo");
//			activities.add(act);
//			act = new Activity(df.parse("03/21/2013 12:00"), 
//					df.parse("03/21/2013 14:00"),
//					null,
//					"Meal break",
//					"Memo");
//			activities.add(act);
//			act = new Activity(df.parse("03/22/2013 12:00"), 
//					df.parse("03/22/2013 14:00"),
//					null,
//					"Meal break",
//					"Memo");
//			activities.add(act);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
	}
	
	@Override
	public List<Activity> getActivities() {
		return new LinkedList<Activity>(activities);
	}

	@Override
	public boolean addActivity(Activity activity) {
        int  i = 0;      //1
        boolean conflicts = false;

        if(activity.getStart().compareTo(activity.getDuration()) > 0){ //2
            return false; //3
        }else{
            while( i < activities.size() )  //4
            {
                if ((activities.get(i).getStart().compareTo(activity.getDuration()) < 0) &&
                        (activity.getStart().compareTo(activities.get(i).getDuration()) < 0)) //5
                    conflicts = true;  //6
                i++; //7
            }
            if ( !conflicts )  //8
            {
                activities.add(activity);  //9
                return true;
            }
            return false; //10

        }
	}

	@Override
	public boolean removeActivity(Activity activity) {
		int index = activities.indexOf(activity);
		if (index<0) return false;
		activities.remove(index);
		return true;
	}

	@Override
	public boolean saveActivities() {
		return true;
	}

	@Override
	public int count() {
		return activities.size();
	}

	@Override
	public List<Activity> activitiesByName(String name) {
		List<Activity> result = new LinkedList<Activity>();
		for (Activity a : activities)
			if (a.getName().equals(name)) result.add(a);
		return result;
	}

	@Override
	public List<Activity> activitiesOnDate(String name, Date d) {
		List<Activity> result = new LinkedList<Activity>();
		for (Activity a : activities)
			if (a.getName().equals(name))
				if (a.getStart().compareTo(d) <= 0 && d.compareTo(a.getDuration()) <= 0 ) result.add(a);
		return result;
	}

}
